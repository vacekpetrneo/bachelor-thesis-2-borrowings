import {ColumnType, Insertable, Selectable} from "kysely";

export interface ItemLogTable {
    item_log_id: ColumnType<number, never, never>,
    item_log_user_id: number,
    item_log_timestamp: ColumnType<Date, Date, never>,
    item_log_item_id: number,
    item_log_item_is_inactive: boolean,
    item_log_item_is_public: boolean,
    item_log_item_room_id: number,
}

export type ItemLog = Selectable<ItemLogTable>;
export type ItemLogInsertData = Insertable<ItemLogTable>;
