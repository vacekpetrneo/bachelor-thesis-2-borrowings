import {ColumnType, Insertable, Selectable, Updateable} from "kysely";

export interface ItemTable {
  item_id: ColumnType<number, never, never>,
  item_is_inactive: ColumnType<boolean, never, boolean>,
  item_is_public: boolean,
  item_room_id: number,
}

export type Item = Selectable<ItemTable>;
export type ItemInsertData = Insertable<ItemTable>;
export type ItemUpdateData = Updateable<ItemTable>;