import {ColumnType, Insertable, Selectable, Updateable} from "kysely";

export interface UserTable {
    user_id: ColumnType<number, never, never>,
    user_is_admin: boolean,
    user_email: string,
}

export type User = Selectable<UserTable>;
export type UserInsertData = Insertable<UserTable>;
export type UserUpdateData = Updateable<UserTable>;