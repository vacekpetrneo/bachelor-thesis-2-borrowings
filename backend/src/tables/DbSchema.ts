import {BorrowingTable} from "./Borrowing";
import {BorrowingLogTable} from "./BorrowingLog";
import {ItemTable} from "./Item";
import {ItemLogTable} from "./ItemLog";
import {ItemTransferTable} from "./ItemTransfer";
import {RoomTable} from "./Room";
import {RoomLogTable} from "./RoomLog";
import {UserTable} from "./User";

export interface DbSchema {
    borrowing: BorrowingTable,
    borrowing_log: BorrowingLogTable,
    item: ItemTable,
    item_log: ItemLogTable,
    item_transfer: ItemTransferTable,
    room: RoomTable,
    room_log: RoomLogTable,
    user: UserTable,
}
