import {ColumnType, Insertable, Selectable, Updateable} from "kysely";

export interface BorrowingTable {
  borrowing_recipient_user_id: ColumnType<number, number, never>,
  borrowing_item_id: ColumnType<number, number, never>,
  borrowing_request_timestamp: ColumnType<Date, Date, never>,
  borrowing_provider_user_id: ColumnType<number | null, never, number | null>,
  borrowing_start_timestamp: ColumnType<Date, never, Date>,
  borrowing_end_planned_timestamp: ColumnType<Date, never, Date>,
}

export type Borrowing = Selectable<BorrowingTable>;
export type BorrowingInsertData = Insertable<BorrowingTable>;
export type BorrowingUpdateData = Updateable<BorrowingTable>;
