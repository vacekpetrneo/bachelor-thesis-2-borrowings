import {ColumnType, Insertable, Selectable} from "kysely";

export interface BorrowingLogTable {
    borrowing_log_id: ColumnType<number, never, never>,
    borrowing_log_recipient_user_id: number,
    borrowing_log_item_id: number,
    borrowing_log_request_timestamp: ColumnType<Date, Date, never>,
    borrowing_log_provider_user_id: number,
    borrowing_log_start_timestamp: ColumnType<Date, Date, never>,
    borrowing_log_end_planned_timestamp: ColumnType<Date, Date, never>,
    borrowing_log_end_real_timestamp: ColumnType<Date, Date, never>,
    borrowing_log_return_recipient_user_id: number,
}

export type BorrowingLog = Selectable<BorrowingLogTable>;
export type BorrowingLogInsertData = Insertable<BorrowingLogTable>;
