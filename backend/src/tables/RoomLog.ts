import {ColumnType, Insertable, Selectable} from "kysely";

export interface RoomLogTable {
    room_log_id: ColumnType<number, never, never>,
    room_log_user_id: number,
    room_log_timestamp: ColumnType<Date, Date, never>,
    room_log_room_id: number,
    room_log_room_is_inactive: boolean,
    room_log_room_curator_user_id: number,
    room_log_room_name: string,
}

export type RoomLog = Selectable<RoomLogTable>;
export type RoomLogInsertData = Insertable<RoomLogTable>;