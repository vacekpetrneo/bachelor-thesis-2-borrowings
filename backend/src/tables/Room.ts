import {ColumnType, Insertable, Selectable, Updateable} from "kysely";

export interface RoomTable {
    room_id: ColumnType<number, never, never>,
    room_is_inactive: ColumnType<boolean, never, boolean>,
    room_curator_user_id: number,
    room_name: string,
}

export type Room = Selectable<RoomTable>;
export type RoomInsertData = Insertable<RoomTable>;
export type RoomUpdateData = Updateable<RoomTable>;
