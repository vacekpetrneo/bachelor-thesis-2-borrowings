import {Insertable, Selectable} from "kysely";

export interface ItemTransferTable {
    item_transfer_item_id: number,
    item_transfer_room_id: number,
}

export type ItemTransfer = Selectable<ItemTransferTable>;
export type ItemTransferInsertData = Insertable<ItemTransferTable>;
