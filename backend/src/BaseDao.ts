import {Kysely, Transaction} from "kysely";
import {DbSchema} from "./tables/DbSchema";

export abstract class BaseDao {
    protected readonly db : Kysely<DbSchema>;

    constructor(db: Kysely<DbSchema>) {
        this.db = db;
    }

    protected readonly BaseDaoTransaction = class {
        protected readonly transaction : Transaction<DbSchema>;

        constructor(transaction: Transaction<DbSchema>) {
            this.transaction = transaction;
        }
    }
}