import {BaseDao} from "../BaseDao";
import {Item, ItemInsertData, ItemUpdateData} from "../tables/Item";
import {ItemLog, ItemLogInsertData} from "../tables/ItemLog";
import {ItemTransfer, ItemTransferInsertData} from "../tables/ItemTransfer";

export class ItemWriteDao extends BaseDao {
    private readonly baseTransactionReference = this.BaseDaoTransaction;
    private readonly ItemWriteDaoTransaction = class extends this.baseTransactionReference {
        public readonly updateItemRow = (
            itemId: number,
            itemRoomId: number,
            itemIsPublic: boolean,
            itemIsInactive: boolean,
        ) : Promise<Item> => {
            const params : ItemUpdateData = {
                item_room_id: itemRoomId,
                item_is_public: itemIsPublic,
                item_is_inactive: itemIsInactive,
            };
            const query = this.transaction.updateTable('item').where('item_id', '=', itemId).set(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly insertItemRow = (itemRoomId: number, itemIsPublic: boolean) : Promise<Item> => {
            const params : ItemInsertData = {
                item_room_id: itemRoomId,
                item_is_public: itemIsPublic,
            };
            const query = this.transaction.insertInto('item').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly insertItemLogRow = (
            itemId: number,
            itemRoomId: number,
            itemIsPublic: boolean,
            itemIsInactive: boolean,
            userId: number,
        ) : Promise<ItemLog> => {
            const params : ItemLogInsertData = {
                item_log_item_id: itemId,
                item_log_item_room_id: itemRoomId,
                item_log_item_is_public: itemIsPublic,
                item_log_item_is_inactive: itemIsInactive,
                item_log_user_id: userId,
                item_log_timestamp: new Date(),
            };
            const query = this.transaction.insertInto('item_log').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly insertItemTransferRow = (itemId : number, targetRoomId : number) : Promise<ItemTransfer> => {
            const params : ItemTransferInsertData = {
                item_transfer_item_id: itemId,
                item_transfer_room_id: targetRoomId,
            }
            const query = this.transaction.insertInto('item_transfer').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly updateItemRoom = (itemId : number, targetRoomId : number) : Promise<Item> => {
            const params : ItemUpdateData = {
                item_room_id: targetRoomId,
            }
            const query = this.transaction.updateTable('item').set(params).where('item_id', '=', itemId).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly deleteItemTransferRow = (itemId : number, targetRoomId : number) : void => {
            this.transaction.deleteFrom('item_transfer')
                    .where('item_transfer_item_id', '=', itemId)
                    .where('item_transfer_room_id', '=', targetRoomId)
                    .executeTakeFirstOrThrow();
        }
    }

    public readonly createItem = async (itemRoomId: number, itemIsPublic: boolean, userId: number) : Promise<Item> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.ItemWriteDaoTransaction(transactionBase);
                const result = await transaction.insertItemRow(itemRoomId, itemIsPublic);
                await transaction.insertItemLogRow(
                    result.item_id,
                    itemRoomId,
                    itemIsPublic,
                    result.item_is_inactive,
                    userId,
                );
                return result;
            }
        );
    }

    public readonly updateItem = async (
        itemId: number,
        itemRoomId: number,
        itemIsPublic: boolean,
        itemIsInactive: boolean,
        userId: number,
    ) : Promise<Item> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.ItemWriteDaoTransaction(transactionBase);
                const result = await transaction.updateItemRow(itemId, itemRoomId, itemIsPublic, itemIsInactive);
                await transaction.insertItemLogRow(itemId, itemRoomId, itemIsPublic, itemIsInactive, userId);
                return result;
            }
        );
    }

    public readonly createItemTransferRequest = async (itemId : number, targetRoomId : number) : Promise<ItemTransfer> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.ItemWriteDaoTransaction(transactionBase);
                const result = await transaction.insertItemTransferRow(itemId, targetRoomId);
                return result;
            }
        )
    }

    public readonly confirmItemTransferRequest = async (itemId : number, targetRoomId : number, userId : number) : Promise<Item> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.ItemWriteDaoTransaction(transactionBase);
                const result = await transaction.updateItemRoom(itemId, targetRoomId);
                await transaction.insertItemLogRow(
                    result.item_id,
                    result.item_room_id,
                    result.item_is_public,
                    result.item_is_inactive,
                    userId,
                );
                await transaction.deleteItemTransferRow(itemId, targetRoomId);
                return result;
            }
        )
    }
}