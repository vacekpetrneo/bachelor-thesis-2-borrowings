import {BaseDao} from "../BaseDao";
import {Room, RoomInsertData, RoomUpdateData} from "../tables/Room";
import {RoomLog, RoomLogInsertData} from "../tables/RoomLog";

export class RoomWriteDao extends BaseDao {
    private readonly baseTransactionReference = this.BaseDaoTransaction;
    private readonly RoomWriteDaoTransaction = class extends this.baseTransactionReference {
        public readonly updateRoomRow = (
            roomId: number,
            roomIsInactive: boolean,
            roomCuratorUserId: number,
            roomName: string,
        ) : Promise<Room> => {
            const params : RoomUpdateData = {
                room_is_inactive: roomIsInactive,
                room_curator_user_id: roomCuratorUserId,
                room_name: roomName,
            };
            const query = this.transaction.updateTable('room').where('room_id', '=', roomId).set(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly insertRoomRow = (roomCuratorUserId: number, roomName: string) : Promise<Room> => {
            const params : RoomInsertData = {
                room_curator_user_id: roomCuratorUserId,
                room_name: roomName,
            };
            const query = this.transaction.insertInto('room').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly insertRoomLogRow = (
            roomId: number,
            roomIsInactive: boolean,
            roomCuratorUserId: number,
            roomName: string,
            userId: number,
        ) : Promise<RoomLog> => {
            const params : RoomLogInsertData = {
                room_log_room_id: roomId,
                room_log_room_is_inactive: roomIsInactive,
                room_log_room_curator_user_id: roomCuratorUserId,
                room_log_room_name: roomName,
                room_log_user_id: userId,
                room_log_timestamp: new Date(),
            };
            const query = this.transaction.insertInto('room_log').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }
    }

    public readonly createRoom = async (
        roomCuratorUserId: number,
        roomName: string,
        userId: number,
    ) : Promise<Room> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.RoomWriteDaoTransaction(transactionBase);
                const result = await transaction.insertRoomRow(roomCuratorUserId, roomName);
                await transaction.insertRoomLogRow(
                    result.room_id,
                    result.room_is_inactive,
                    roomCuratorUserId,
                    roomName,
                    userId
                );
                return result;
            }
        );
    }

    public readonly updateRoom = async (
        roomId: number,
        roomIsInactive: boolean,
        roomCuratorUserId: number,
        roomName: string,
        userId: number,
    ) : Promise<Room> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.RoomWriteDaoTransaction(transactionBase);
                const result = await transaction.updateRoomRow(roomId, roomIsInactive, roomCuratorUserId, roomName);
                await transaction.insertRoomLogRow(roomId, roomIsInactive, roomCuratorUserId, roomName, userId);
                return result;
            }
        );
    }
}