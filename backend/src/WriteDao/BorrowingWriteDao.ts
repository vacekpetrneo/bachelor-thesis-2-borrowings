import {BaseDao} from "../BaseDao";
import {Borrowing, BorrowingInsertData} from "../tables/Borrowing";
import {BorrowingLog} from "../tables/BorrowingLog";

export class BorrowingWriteDao extends BaseDao {
    private readonly baseTransactionReference = this.BaseDaoTransaction;
    private readonly BorrowingWriteDaoTransaction = class extends this.baseTransactionReference {
        public readonly createBorrowingRow = async (
            itemId : number,
            recipientUserId : number,
        ) : Promise<Borrowing> => {
            const params : BorrowingInsertData = {
                borrowing_item_id: itemId,
                borrowing_recipient_user_id: recipientUserId,
                borrowing_request_timestamp: new Date(),
            };
            const query = await this.transaction.insertInto('borrowing').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly deleteBorrowingRow = async (itemId : number, recipientUserId : number) : Promise<Borrowing> => {
            const query = this.transaction.deleteFrom('borrowing')
                    .where('borrowing.borrowing_item_id', '=', itemId)
                    .where('borrowing.borrowing_recipient_user_id', '=', recipientUserId)
                    .returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly lendBorrowingRow = async (
                itemId : number,
                recipientUserId : number,
                providerUserId: number,
                endPlanned: Date,
        ) : Promise<Borrowing> => {
            const params = {
                borrowing_end_planned_timestamp : endPlanned,
                borrowing_provider_user_id : providerUserId,
                borrowing_start_timestamp : new Date(),
            };
            const query = this.transaction.updateTable('borrowing')
                    .where('borrowing.borrowing_item_id', '=', itemId)
                    .where('borrowing.borrowing_recipient_user_id', '=', recipientUserId)
                    .set(params)
                    .returningAll();
            return query.executeTakeFirstOrThrow();
        }

        public readonly createBorrowingLogRow = async (
                itemId : number,
                recipientUserId : number,
                requestDate : Date,
                startDate : Date,
                providerUserId : number,
                endPlannedDate : Date,
                returnRecipientUserId : number,
        ) : Promise<BorrowingLog> => {
            const params = {
                borrowing_log_item_id : itemId,
                borrowing_log_recipient_user_id : recipientUserId,
                borrowing_log_request_timestamp : requestDate,
                borrowing_log_start_timestamp : startDate,
                borrowing_log_provider_user_id : providerUserId,
                borrowing_log_end_planned_timestamp : endPlannedDate,
                borrowing_log_return_recipient_user_id : returnRecipientUserId,
                borrowing_log_end_real_timestamp : new Date(),
            }
            const query = this.transaction.insertInto('borrowing_log').values(params).returningAll();
            return query.executeTakeFirstOrThrow();
        }
    }

    public readonly createBorrowingRequest = async (itemId : number, recipientUserId : number) : Promise<Borrowing> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.BorrowingWriteDaoTransaction(transactionBase);
                const result = await transaction.createBorrowingRow(itemId, recipientUserId);
                return result;
            }
        );
    }

    public readonly lendBorrowing = async (
        itemId : number,
        recipientUserId : number,
        providerUserId : number,
        endPlanned : Date,
    ) : Promise<Borrowing> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.BorrowingWriteDaoTransaction(transactionBase);
                const result = transaction.lendBorrowingRow(itemId, recipientUserId, providerUserId, endPlanned);
                return result;
            }
        )
    }

    public readonly archiveBorrowing = async (
            itemId : number,
            recipientUserId : number,
            returnRecipientUserId : number,
    ) : Promise<BorrowingLog> => {
        return await this.db.transaction().execute(
            async (transactionBase) => {
                const transaction = new this.BorrowingWriteDaoTransaction(transactionBase);
                const borrowing = await transaction.deleteBorrowingRow(itemId, recipientUserId)
                const result = await transaction.createBorrowingLogRow(
                    borrowing.borrowing_item_id,
                    borrowing.borrowing_recipient_user_id,
                    borrowing.borrowing_request_timestamp,
                    borrowing.borrowing_start_timestamp,
                    borrowing.borrowing_provider_user_id || returnRecipientUserId,
                    borrowing.borrowing_end_planned_timestamp,
                    returnRecipientUserId,
                );
                return result;
            }
        )
    }
}