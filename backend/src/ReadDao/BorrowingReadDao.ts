import {BaseDao} from "../BaseDao";
import {Borrowing} from "../tables/Borrowing";

export class BorrowingReadDao extends BaseDao {
    public readonly getBorrowingByItemIdAndRecipientUserId = async (
            itemId : number,
            recipientUserId : number,
    ) : Promise<undefined | Borrowing> => {
        return await this.db.selectFrom('borrowing')
                .where('borrowing.borrowing_item_id', '=', itemId)
                .where('borrowing.borrowing_recipient_user_id', '=', recipientUserId)
                .selectAll()
                .executeTakeFirst();
    }

    public readonly getActiveBorrowingByItemId = async (itemId: number) : Promise<undefined | Borrowing> => {
        return await this.db.selectFrom('borrowing')
                .where('borrowing.borrowing_item_id', '=', itemId)
                .where('borrowing.borrowing_start_timestamp', 'is not', null)
                .selectAll()
                .executeTakeFirst();
    }
}