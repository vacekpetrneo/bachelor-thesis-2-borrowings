import {BaseDao} from "../BaseDao";
import {Room} from "../tables/Room";

export class RoomReadDao extends BaseDao {
    public readonly getAllRooms = async () : Promise<Room[]> => {
        return await this.db.selectFrom("room").selectAll().execute();
    }

    public readonly getRoomById = async (roomId: number) : Promise<Room | undefined> => {
        return await this.db.selectFrom("room").selectAll().where("room.room_id", "=", roomId).executeTakeFirst();
    }

    public readonly getRoomIdsByCuratorUserId = async (curatorUserId : number) : Promise<number[]> => {
        const rowSet : {room_id: number}[] = await this.db.selectFrom("room")
                .select('room_id')
                .where('room.room_curator_user_id', '=', curatorUserId)
                .execute();
        return rowSet.map((row : {room_id: number}) => row.room_id);
    }
}