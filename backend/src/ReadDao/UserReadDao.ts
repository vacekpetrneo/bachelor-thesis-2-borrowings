import {BaseDao} from "../BaseDao";
import {User} from "../tables/User";

export class UserReadDao extends BaseDao {
    public readonly getUserById = async (userId: number) : Promise<User | undefined> => {
        return await this.db.selectFrom("user")
            .where('user_id', '=', userId)
            .selectAll()
            .executeTakeFirst();
    }
}