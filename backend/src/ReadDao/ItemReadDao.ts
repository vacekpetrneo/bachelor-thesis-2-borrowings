import {BaseDao} from "../BaseDao";
import {Item} from "../tables/Item";
import {ItemTransfer} from "../tables/ItemTransfer";

export class ItemReadDao extends BaseDao {
    public readonly getAllItems = async (publicOnly: boolean = true) : Promise<Item[]> => {
        const query = this.db.selectFrom("item").selectAll();
        if (publicOnly) {
            query.where('item_is_public', '=', true);
        }
        return await query.execute();
    }

    public readonly getItemById = async (itemId: number, publicOnly: boolean = true) : Promise<Item | undefined> => {
        const query = this.db.selectFrom("item").selectAll().where("item_id", "=", itemId);
        if (publicOnly) {
            query.where('item_is_public', '=', true);
        }
        return await query.executeTakeFirst();
    }

    public readonly getItemsByRoomId = async (roomId: number, publicOnly: boolean = true) : Promise<Item[]> => {
        const query = this.db.selectFrom("item").selectAll().where("item_room_id", "=", roomId);
        if (publicOnly) {
            query.where('item_is_public', '=', true);
        }
        return await query.execute();
    }

    public readonly getItemTransferRequest = async (itemId: number, targetRoomId: number) : Promise<ItemTransfer | undefined> => {
        const query = this.db.selectFrom("item_transfer").selectAll()
                .where("item_transfer_item_id", "=", itemId)
                .where("item_transfer_room_id", "=", targetRoomId)
        return await query.executeTakeFirst();
    }
}