import express from "express";

import {ItemService} from "./services/ItemService";
import {RoomService} from "./services/RoomService";
import {ItemTransferService} from "./services/ItemTransferService";
import {BorrowingService} from "./services/BorrowingService";
import {Repository} from "./Repository";
import {db} from "./db";

const app = express();
const port = 3000;

const repo = new Repository(db);

app.use(express.json())
app.get("/", (req, res) => {
    res.send('Hello world!');
});

const errorHandleWrapper = function (fun : any) {
    return function(...params : any[]) {
        fun(...params).catch((error : any) => {
            console.log(error);
        });
    }
}

const roomService = new RoomService(repo);
app.get("/room/list", errorHandleWrapper(roomService.list));
app.get("/room/get/roomId/:roomId", errorHandleWrapper(roomService.get));
app.post("/room/create", errorHandleWrapper(roomService.create));
app.post("/room/update/roomId/:roomId", errorHandleWrapper(roomService.update))

const itemService = new ItemService(repo);
app.get("/item/list", errorHandleWrapper(itemService.list));
app.get("/item/get/itemId/:itemId", errorHandleWrapper(itemService.getByItemId));
app.post("/item/create", errorHandleWrapper(itemService.create));
app.post("/item/update/itemId/:itemId", errorHandleWrapper(itemService.update));

const itemTransferService = new ItemTransferService(repo);
app.post(
    "/itemTransfer/request/itemId/:itemId/targetRoomId/:targetRoomId",
    errorHandleWrapper(itemTransferService.itemTransferRequest)
);
app.post(
    "/itemTransfer/request/targetRoomId/:targetRoomId/itemId/:itemId",
    errorHandleWrapper(itemTransferService.itemTransferRequest)
);
app.post(
    "/itemTransfer/accept/itemId/:itemId/targetRoomId/:targetRoomId",
    errorHandleWrapper(itemTransferService.itemTransferAccept)
);
app.post(
    "/itemTransfer/accept/targetRoomId/:targetRoomId/itemId/:itemId",
    errorHandleWrapper(itemTransferService.itemTransferAccept)
);

const borrowingService = new BorrowingService(repo);
app.post("/borrowing/request/itemId/:itemId", errorHandleWrapper(borrowingService.bookBorrowing));
app.post("/borrowing/lend/itemId/:itemId", errorHandleWrapper(borrowingService.lendBorrowing));
app.post("/borrowing/return/itemId/:itemId", errorHandleWrapper(borrowingService.returnBorrowing));

app.listen(port, () => {
    console.log(`The app is listening on port ${port}`);
});
