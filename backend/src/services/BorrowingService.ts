import {Request, Response} from "express-serve-static-core";

import {BaseService} from "./BaseService";

export class BorrowingService extends BaseService {
    private readonly DEFAULT_BORROWING_INTERVAL_MILLISECONDS = 31 * 24 * 60 * 60 * 1000; // 31 days

    public bookBorrowing = async (req: Request, res: Response) : Promise<void> => {
        const item = await this.parseItemOrThrow(req.params.itemId, false);
        const recipientUser = await this.parseUserOrThrow(this.getUserId());

        const borrowing = await this.repo.getBorrowingReadDao().getBorrowingByItemIdAndRecipientUserId(
            item.item_id,
            recipientUser.user_id,
        );
        if (borrowing) {
            res.json(borrowing);
            return;
        }

        const result = await this.repo.getBorrowingWriteDao().createBorrowingRequest(
            item.item_id,
            recipientUser.user_id,
        );
        res.json(result)
    };

    public lendBorrowing = async (req: Request, res: Response) : Promise<void> => {
        const item = await this.parseItemOrThrow(req.params.itemId, false);
        const recipientUser = await this.parseUserOrThrow(req.body.recipient_user_id);
        const providerUser = await this.parseUserOrThrow(this.getUserId());

        if (!providerUser.user_is_admin) {
            const room = await this.repo.getRoomReadDao().getRoomById(item.item_room_id);
            if (room?.room_curator_user_id !== providerUser.user_id) {
                throw new Error('You are not authorised to lend the item.');
            }
        }

        const borrowing = await this.repo.getBorrowingReadDao().getBorrowingByItemIdAndRecipientUserId(
            item.item_id,
            recipientUser.user_id,
        );
        if (!borrowing) {
            const message = 'Request to borrow item #' + item.item_id
                    + ' by user #' + recipientUser.user_id + ' not found. Cannot lend the item.';
            throw new Error(message);
        }

        const activeBorrowing = await this.repo.getBorrowingReadDao().getActiveBorrowingByItemId(item.item_id);
        if (activeBorrowing) {
            throw new Error('Item #' + item.item_id + ' is currently borrowed by someone else. Cannot lend the item.');
        }

        const result = await this.repo.getBorrowingWriteDao().lendBorrowing(
            item.item_id,
            recipientUser.user_id,
            providerUser.user_id,
            new Date(Date.now() + this.DEFAULT_BORROWING_INTERVAL_MILLISECONDS)
        );
        res.json(result)
    };

    public returnBorrowing = async (req: Request, res: Response) : Promise<void> => {
        const item = await this.parseItemOrThrow(req.params.itemId, false);
        const recipientUser = await this.parseUserOrThrow(req.body.recipient_user_id);
        const returnRecipientUser = await this.parseUserOrThrow(this.getUserId());

        const curatedRoomIds = await this.repo.getRoomReadDao().getRoomIdsByCuratorUserId(returnRecipientUser.user_id);
        if (curatedRoomIds.length < 1) {
            throw new Error('You are not authorised to mark the item as returned.');
        }

        const borrowing = await this.repo.getBorrowingReadDao().getBorrowingByItemIdAndRecipientUserId(
            item.item_id,
            recipientUser.user_id,
        );
        if (!borrowing) {
            const message = 'Borrowing record of item #' + item.item_id
                    + ' by user #' + recipientUser.user_id + ' not found. Cannot return the item.';
            throw new Error(message);
        }

        if (!borrowing.borrowing_start_timestamp) {
            throw new Error('The item was not lent yet. Cannot return the item.');
        }

        const result = await this.repo.getBorrowingWriteDao().archiveBorrowing(
            item.item_id,
            recipientUser.user_id,
            returnRecipientUser.user_id
        );
        res.json(result)
    };
}
