import {Request, Response} from "express-serve-static-core";

import {BaseService} from "./BaseService";
import {User} from "../tables/User";

export class RoomService extends BaseService {
    public readonly list = (req: Request, res: Response) : void => {
        const result = this.repo.getRoomReadDao().getAllRooms();
        res.json(result);
    };

    public readonly get = (req: Request, res: Response) : void => {
        const roomId = Number(req.params.roomId);
        const result = this.repo.getRoomReadDao().getRoomById(roomId);
        res.json(result);
    };

    public readonly create = async (req: Request, res: Response) : Promise<void> => {
        const adminUser = await this.parseAdminUserOrThrow(this.getUserId());
        const curatorUser = await this.parseUserOrThrow(req.body.room_curator_user_id);

        const result = await this.repo.getRoomWriteDao().createRoom(
            curatorUser.user_id,
            req.body.room_name,
            adminUser.user_id,
        );
        res.json(result);
    };

    public readonly update = async (req: Request, res: Response) : Promise<void> => {
        const adminUser = await this.parseAdminUserOrThrow(this.getUserId());
        const curatorUser = await this.parseUserOrThrow(req.body.room_curator_user_id);
        const room = await this.parseRoomOrThrow(req.params.roomId);

        const result = this.repo.getRoomWriteDao().updateRoom(
            room.room_id,
            Boolean(req.body.room_is_inactive),
            curatorUser.user_id,
            req.body.room_name,
            adminUser.user_id,
        );
        res.json(result);
    };

    private readonly parseAdminUserOrThrow = async (userIdRaw: any) : Promise<User> => {
        const user = await this.parseUserOrThrow(userIdRaw);
        if (!user.user_is_admin) {
            throw new Error('You are not authorised to manipulate room entries.');
        }
        return user;
    }
}
