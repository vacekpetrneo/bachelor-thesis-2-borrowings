import {Request, Response} from "express-serve-static-core";

import {BaseService} from "./BaseService";

export class ItemService extends BaseService {
    public readonly list = async (req: Request, res: Response) : Promise<void> => {
        //TODO paging
        const publicOnly = this.getUserId() === undefined;
        const result = await this.repo.getItemReadDao().getAllItems(publicOnly);
        res.json(result);
    };

    public readonly getByItemId = async (req: Request, res: Response) : Promise<void> => {
        const publicOnly = this.getUserId() === undefined;
        const item = await this.parseItemOrThrow(req.params.itemId, publicOnly);
        res.json(item);
    };

    public readonly create = async (req: Request, res: Response) : Promise<void> => {
        const room = await this.parseRoomOrThrow(req.body.item_room_id);
        const user = await this.parseUserOrThrow(this.getUserId());
        if (room.room_curator_user_id !== user.user_id && !user.user_is_admin) {
            throw new Error('You are not authorised to manage item entries in roomId: ' + room.room_id);
        }

        const result = await this.repo.getItemWriteDao().createItem(
            room.room_id,
            Boolean(req.body.item_is_public),
                user.user_id,
        );
        res.json(result);
    };

    public readonly update = async (req: Request, res: Response) : Promise<void> => {
        const room = await this.parseRoomOrThrow(req.body.item_room_id);
        const user = await this.parseUserOrThrow(this.getUserId());
        if (room.room_curator_user_id !== user.user_id && !user.user_is_admin) {
            throw new Error('You are not authorised to manage item entries in roomId: ' + room.room_id);
        }
        const item = await this.parseItemOrThrow(req.params.itemId, false);

        const activeBorrowing = await this.repo.getBorrowingReadDao().getActiveBorrowingByItemId(item.item_id);
        if (activeBorrowing) {
            const message = 'Item #' + item.item_id
                    + ' is currently borrowed. It needs to be returned before you can update.'
            throw new Error(message);
        }

        const result = this.repo.getItemWriteDao().updateItem(
            item.item_id,
            item.item_room_id, //item_room_id should only be changed via transfer item feature
            Boolean(req.body.item_is_public),
            Boolean(req.body.item_is_public),
            user.user_id,
        );
        res.json(result)
    };
}
