import {Request, Response} from "express-serve-static-core";

import {BaseService} from "./BaseService";

export class ItemTransferService extends BaseService {
    public readonly itemTransferRequest = async (req: Request, res: Response) : Promise<void> => {
        const item = await this.parseItemOrThrow(req.params.itemId);
        const targetRoom = await this.parseRoomOrThrow(req.params.targetRoomId);
        const userId = this.getUserId();
        if (userId === undefined || userId < 1) {
            throw new Error('You are not authorized to create a transfer request for this item.');
        }

        const curatedRoomIds = await this.repo.getRoomReadDao().getRoomIdsByCuratorUserId(userId);
        if (!curatedRoomIds.includes(item.item_room_id)) {
            throw new Error('You are not authorized to create a transfer request for this item.');
        }

        const itemRequest = await this.repo.getItemReadDao().getItemTransferRequest(item.item_id, targetRoom.room_id);
        if (itemRequest) {
            res.json(itemRequest);
            return;
        }

        const result = await this.repo.getItemWriteDao().createItemTransferRequest(item.item_id, targetRoom.room_id);
        res.json(result);
    };

    public readonly itemTransferAccept = async (req: Request, res: Response) : Promise<void> => {
        const item = await this.parseItemOrThrow(req.params.itemId);
        const targetRoom = await this.parseRoomOrThrow(req.params.targetRoomId);
        const userId = this.getUserId();
        if (
            userId === undefined
            || userId < 1
            || userId !== targetRoom.room_curator_user_id
        ) {
            throw new Error('You are not authorized to accept this item transfer.');
        }

        const itemRequest = await this.repo.getItemReadDao().getItemTransferRequest(item.item_id, targetRoom.room_id);
        if (itemRequest) {
            throw new Error('No request found to transfer item #' + item.item_id + ' to room #' + targetRoom.room_id + '.');
        }

        const result = await this.repo.getItemWriteDao().confirmItemTransferRequest(item.item_id, targetRoom.room_id, userId);
        res.json(result)
    };
}
