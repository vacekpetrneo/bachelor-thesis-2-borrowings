import {Repository} from "../Repository";
import {Item} from "../tables/Item";
import {Room} from "../tables/Room";
import {User} from "../tables/User";

export abstract class BaseService {
    protected repo: Repository;

    constructor(repo: Repository) {
        this.repo = repo;
    }

    //TODO
    protected getUserId() : number | undefined {
        return 1;
    }

    protected readonly parseIdOrThrow = (idRaw: any, errorMessage: string) : number => {
        const id = Number(idRaw);
        if (id < 1 || !Number.isInteger(id)) {
            throw new Error(errorMessage);
        }
        return id;
    }

    protected readonly parseItemOrThrow = async (itemIdRaw: any, publicOnly: boolean = true) : Promise<Item> => {
        const errorMessage = 'Invalid itemId: ' + itemIdRaw;
        const itemId = this.parseIdOrThrow(itemIdRaw, errorMessage);
        const item = await this.repo.getItemReadDao().getItemById(itemId, publicOnly);
        if (item === undefined) {
            throw new Error(errorMessage);
        }
        return item;
    }

    protected readonly parseRoomOrThrow = async (roomIdRaw: any) : Promise<Room> => {
        const errorMessage = 'Invalid roomId: ' + roomIdRaw;
        const roomId = this.parseIdOrThrow(roomIdRaw, errorMessage);
        const room = await this.repo.getRoomReadDao().getRoomById(roomId);
        if (room === undefined) {
            throw new Error(errorMessage);
        }
        return room;
    }

    protected readonly parseUserOrThrow = async (userIdRaw: any) : Promise<User> => {
        const errorMessage = 'Invalid userId: ' + userIdRaw;
        const userId = this.parseIdOrThrow(userIdRaw, errorMessage);
        const user = await this.repo.getUserReadDao().getUserById(userId);
        if (user === undefined) {
            throw new Error(errorMessage);
        }
        return user;
    }
}