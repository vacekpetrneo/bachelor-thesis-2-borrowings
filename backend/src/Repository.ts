import {Kysely} from "kysely";

import {DbSchema} from "./tables/DbSchema";
import {BorrowingReadDao} from "./ReadDao/BorrowingReadDao";
import {BorrowingWriteDao} from "./WriteDao/BorrowingWriteDao";
import {ItemReadDao} from "./ReadDao/ItemReadDao";
import {ItemWriteDao} from "./WriteDao/ItemWriteDao";
import {RoomReadDao} from "./ReadDao/RoomReadDao";
import {RoomWriteDao} from "./WriteDao/RoomWriteDao";
import {UserReadDao} from "./ReadDao/UserReadDao";

export class Repository {
    private readonly db;

    constructor(db: Kysely<DbSchema>) {
        this.db = db;
    }

    public getBorrowingReadDao = () : BorrowingReadDao => {
        const borrowingReadDaoInstance = new BorrowingReadDao(this.db);
        this.getBorrowingReadDao = () : BorrowingReadDao => borrowingReadDaoInstance;
        return borrowingReadDaoInstance;
    }

    public getBorrowingWriteDao = () : BorrowingWriteDao => {
        const borrowingWriteDaoInstance = new BorrowingWriteDao(this.db);
        this.getBorrowingWriteDao = () : BorrowingWriteDao => borrowingWriteDaoInstance;
        return borrowingWriteDaoInstance;
    }

    public getItemReadDao = () : ItemReadDao => {
        const itemReadDaoInstance = new ItemReadDao(this.db);
        this.getItemReadDao = () : ItemReadDao => itemReadDaoInstance;
        return itemReadDaoInstance;
    }

    public getItemWriteDao = () : ItemWriteDao => {
        const itemWriteDaoInstance = new ItemWriteDao(this.db);
        this.getItemWriteDao = () : ItemWriteDao => itemWriteDaoInstance;
        return itemWriteDaoInstance;
    }

    public getRoomReadDao = () : RoomReadDao => {
        const roomReadDaoInstance = new RoomReadDao(this.db);
        this.getRoomReadDao = () : RoomReadDao => roomReadDaoInstance;
        return roomReadDaoInstance;
    }

    public getRoomWriteDao = () : RoomWriteDao => {
        const roomWriteDaoInstance = new RoomWriteDao(this.db);
        this.getRoomWriteDao = () : RoomWriteDao => roomWriteDaoInstance;
        return roomWriteDaoInstance;
    }

    public getUserReadDao = () : UserReadDao => {
        const userReadDaoInstance = new UserReadDao(this.db);
        this.getUserReadDao = () : UserReadDao => userReadDaoInstance
        return userReadDaoInstance;
    }
}