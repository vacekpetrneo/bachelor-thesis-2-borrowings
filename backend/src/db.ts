import {Kysely, PostgresDialect} from "kysely";
import {Pool} from "pg";
import {DbSchema} from "./tables/DbSchema";
import {config} from "./config";


export const db = new Kysely<DbSchema>({
    dialect: new PostgresDialect({
        pool: new Pool(config)
    })
})

