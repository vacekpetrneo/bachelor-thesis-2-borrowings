CREATE TABLE public."user" (
	user_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	user_is_admin bool DEFAULT false NOT NULL,
	user_email varchar(256) NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY (user_id)
);

CREATE TABLE public.room (
	room_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	room_is_inactive bool NOT NULL DEFAULT false,
	room_curator_user_id int4 NOT NULL,
	room_name varchar NOT NULL,
	CONSTRAINT room_pk PRIMARY KEY (room_id),
	CONSTRAINT room_fk_room_curator_user_id FOREIGN KEY (room_curator_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.room_log (
	room_log_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	room_log_user_id int4 NOT NULL,
	room_log_timestamp timestamp NOT NULL,
	room_log_room_id int4 NOT NULL,
	room_log_room_is_inactive bool NOT NULL,
	room_log_room_curator_user_id int4 NOT NULL,
	room_log_room_name varchar NOT NULL,
	CONSTRAINT room_log_pk PRIMARY KEY (room_log_id),
	CONSTRAINT room_log_fk_room_log_curator_user_id FOREIGN KEY (room_log_room_curator_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT room_log_fk_room_log_room_id FOREIGN KEY (room_log_room_id) REFERENCES public.room(room_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT room_log_fk_room_log_user_id FOREIGN KEY (room_log_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.item (
	item_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	item_is_inactive bool NOT NULL DEFAULT false,
	item_is_public bool NOT NULL,
	item_room_id int4 NOT NULL,
	CONSTRAINT item_pk PRIMARY KEY (item_id),
	CONSTRAINT item_fk_item_room_id FOREIGN KEY (item_room_id) REFERENCES public.room(room_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.item_transfer (
	item_transfer_item_id int4 NOT NULL,
	item_transfer_room_id int4 NOT NULL,
	CONSTRAINT item_transfer_pk PRIMARY KEY (item_transfer_item_id),
	CONSTRAINT item_transfer_fk_item_transfer_item_id FOREIGN KEY (item_transfer_item_id) REFERENCES public.item(item_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT item_transfer_fk_item_transfer_room_id FOREIGN KEY (item_transfer_room_id) REFERENCES public.room(room_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.item_log (
	item_log_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	item_log_user_id int4 NOT NULL,
	item_log_timestamp timestamp NOT NULL,
	item_log_item_id int4 NOT NULL,
	item_log_item_is_inactive bool NOT NULL,
	item_log_item_is_public bool NOT NULL,
	item_log_item_room_id int4 NOT NULL,
	CONSTRAINT item_log_pk PRIMARY KEY (item_log_id),
	CONSTRAINT item_log_fk_item_log_item_room_id FOREIGN KEY (item_log_item_room_id) REFERENCES public.room(room_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT item_log_fk_item_log_user_id FOREIGN KEY (item_log_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.borrowing (
	borrowing_recipient_user_id int4 NOT NULL,
	borrowing_item_id int4 NOT NULL,
	borrowing_request_timestamp timestamp NOT NULL,
	borrowing_provider_user_id int4 NULL,
	borrowing_start_timestamp timestamp NULL,
	borrowing_end_planned_timestamp timestamp NULL,
	CONSTRAINT borrowing_pk PRIMARY KEY (borrowing_recipient_user_id, borrowing_item_id),
	CONSTRAINT borrowing_fk_borrowing_item_id FOREIGN KEY (borrowing_item_id) REFERENCES public.item(item_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT borrowing_fk_borrowing_provider_user_id FOREIGN KEY (borrowing_provider_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT borrowing_fk_borrowing_recipient_user_id FOREIGN KEY (borrowing_recipient_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE public.borrowing_log (
	borrowing_log_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	borrowing_log_recipient_user_id int4 NOT NULL,
	borrowing_log_item_id int4 NOT NULL,
	borrowing_log_request_timestamp timestamp NOT NULL,
	borrowing_log_provider_user_id int4 NOT NULL,
	borrowing_log_start_timestamp timestamp NOT NULL,
	borrowing_log_end_planned_timestamp timestamp NOT NULL,
	borrowing_log_end_real_timestamp timestamp NOT NULL,
	borrowing_log_return_recipient_user_id int4 NOT NULL,
	CONSTRAINT borrowing_log_pk PRIMARY KEY (borrowing_log_id),
	CONSTRAINT borrowing_log_fk_borrowing_log_item_id FOREIGN KEY (borrowing_log_item_id) REFERENCES public.item(item_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT borrowing_log_fk_borrowing_log_provider_user_id FOREIGN KEY (borrowing_log_provider_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT borrowing_log_fk_borrowing_log_recipient_user_id FOREIGN KEY (borrowing_log_recipient_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT borrowing_log_fk_borrowing_log_return_recipient_user_id FOREIGN KEY (borrowing_log_return_recipient_user_id) REFERENCES public."user"(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

